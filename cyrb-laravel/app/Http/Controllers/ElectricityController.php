<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\Register;
use App\Models\bill;


class ElectricityController extends Controller
{
    //

    /*******************
*@function name:check
*@function:login page(admin and user)
*******************/ 

public function check(Request $req)
{
  $req->validate([ 'email'=>'required',
                  'password'=>'required',
                  ]);
  $email=$req->input('email');
  $pass=$req->input('password');
  $userinfo=Register::where('email','=',$req->email)->first();
   if(!$userinfo)
   {
     return back()->with('fail','we cannot  recognize email'); 
   }
   else{
      if(Hash::check($req->password,$userinfo->password))
      {
         
         $req->session()->put('LoggedUser',$userinfo->id);
         $data=['LoggedUserInfo'=>Register::where('id','=',session('LoggedUser'))->first()];
         if($email=='admin@gmail.com'&& $pass=='Admin@123')
         {
         return view('admindashboard',$data);
      }
      else{
        return view('userdashboard',$data);
      }
     }
      else{
        return back()->with('fail','invalid password'); 
      }

}

}
/*******************
*@function name:register
*@function:registere page
*******************/ 

public function register(Request $req)
{
    
    $reg=new Register;
    $reg->name=$req->name;
    $reg->consumer_no=$req->consumer_no;
    $reg->address=$req->address;
    $reg->phno=$req->phno;
    $reg->energy_charge=$req->energy_charge;
    $reg->duty=$req->duty;
    $reg->fixed_charge=$req->fixed_charge;
    $reg->meter_rent=$req->meter_rent;
    $reg->meterrent_stateGST=$req->meterrent_stateGST;
    $reg->meterrent_centralGST=$req->meterrent_centralGST;
    $reg->tariff=$req->tariff;
    $reg->purpose=$req->purpose;
    $reg->billing_cycle=$req->billing_cycle;
    $reg->consumed_units=$req->consumed_units;
    $reg->phase=$req->phase;
    $reg->email=$req->email;
    $reg->password=Hash::make($req->password);
    $query=$reg->save();
    if($query)
    {
       return back()->with('success','successfully registered');
    }
    else{
     return back()->with('fail','something went wrong');
    }
    

}
/*******************
*@function name:dataview
*@function:registered user page  view by admin
*******************/ 

public function dataview(){
        
    /* $data= Register::all();
    return view('viewreguser',['user'=>$data]);*/
     return Register::all();
 }

 /*******************
*@function name:recordview
*@function:viewing single  record
*******************/
 
public function recordview(Request $req)
{
$id=$req->session()->get('LoggedUser');
$data=DB::table('registers')
  ->select('registers.consumer_no','registers.name','registers.address','registers.phno','registers.energy_charge','registers.duty','registers.fixed_charge',
  'registers.meter_rent','registers.meterrent_stateGST','registers.meterrent_centralGST','registers.tariff','registers.purpose','registers.billing_cycle','registers.consumed_units','registers.phase','bills.total_amount')
  ->join('registers', 'registers.id', '=', 'bills.id')
  ->where('bills.id', $id)
 //->select('registers.id as regid','bills.id as billid')
  ->get();
  return view('singlerecord',compact('data'));

  
}
/*******************
*@function name:singleview
*
*******************/

public function singleview(Request $req)
{
    $id=$req->session()->get('LoggedUser');
   
    
    $data=DB::table('bills')
            ->where('id',$id)
            ->get();
    
    $userdetail=DB::table("registers")
        ->where('id',$id)
        ->get();
   
  return view('viewsingle',['view'=>$data,'user'=>$userdetail]);
}


function logout()
 {
    if(session()->has('LoggedUser'))
    {
       session()->pull('LoggedUser');
       return redirect("index");
    }
 }

 /*******************
*@function:bill receipt
*******************/
 public function index()
   {
      $data['bills'] = DB::table('bills')->get();
       return view("Bill",$data);
   }
 
   public function getAmount()
   {
      $getAmount = $_GET['id'];
      $amount  = DB::table('bills')->where('id', $getAmount)->get();
      return Response::json($amount);
   }  
   
   
   /*******************
*@function:preview
*******************/
public function preview()
{
    $data= bill::all();
    return view('preview',['user'=>$data]);
    //return view('preview');
}

public function billcalc()
{
    $data= bill::all();
    return view('billcalc',['user'=>$data]);
    
}
public function singlerecord($id) {

    $record = record::findOrFail($id);
    return view('records.singlerecord',['record' => $record]);
}


/*******************
*@function name:bill
*@function:bill page
*******************/ 

public function dataviews(Request $req)
{
    
    $bil=new bill;
    $bil->tariff=$req->tariff;
    $bil->purpose=$req->purpose;
    $bil->billing_cycle=$req->billing_cycle;
    $bil->consumed_units=$req->consumed_units;
    $bil->phase=$req->phase;
    $query= $bil->save();
    if($query)
    {
       return back()->with('success','successfully added');
    }
    else{
     return back()->with('fail','something went wrong');
    }
    

}

/*******************
*@function name:dataviews
*@function:bill  page  view by admin
*******************/ 

public function bill(){
        
    $data= Bill::all();
    return view('viewbill',['user'=>$data]);
 }

/***public function display(Request $req)
 {
     $user=Register::find($req->id)
     return view('display',compact('user'))
 }**/
}