<!DOCTYPE html>
<html>
    <head>
        <title>ELECTRICITY</title>
            <meta charset=utf-8>
            <meta name="viewport" content="width=device-width,initial-scale=1">
            <!---Fontawesome--->
            <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
            <!---Bootstrap5----->
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <!---custom style---->
            <link rel="stylesheet" href="{{asset('css/index_style.css')}}">
    </head>
    <style>
    </style>
    <body>
@yield('content')
        <!------header section---->
<section>
<header>
    <nav class="back">
        <div class="container-fluid top ">
            <div class="row">
                <div class="col-7">
                    <a href="#"class="text-decoration-none  text-dark">Home</a>
                    <a href="#" class="text-decoration-none  text-dark">Terms</a>
                </div>
            <div class="col-5 text-end">
                    <i class="fab fa-facebook text-dark"></i>
                    <i class="fab fa-instagram text-dark"></i>
                    <i class="fab fa-youtube text-dark"></i>
                    <i class="fab fa-google text-dark"></i>
                </div>
            </div>
        </div>
    </nav>

    
</header>
<!------menu section----->
<nav class="navbar navbar-expand-lg top1 front">
        <div class="container">
            <a href="#" class="text-decoration-none text-dark"><h4>ELECTRICITY BILL </h4></a>
                <ul class="navbar-nav">
               <!--- <li class="nav-item"><a href="{{ url('index') }}" class="nav-link  text-dark">Home</a></li>
                <li class="nav-item"><a href="{{ url('billcalc') }}" class="nav-link  text-dark">Preview</a></li>
                <li class="nav-item"><a href="{{ url('/getPrice/{id}') }}" class="nav-link  text-dark">Invoice</a></li>-->
                <li class="nav-item"><a href="{{ url('tableview') }}" class="nav-link  text-dark">View details</a></li>
               <!-- <li class="nav-item"><a href="{{ url('') }}" class="nav-link  text-dark">Bill Calculator</a></li>
                <li class="nav-item"><a href="{{ url('bilview') }}" class="nav-link  text-dark">View Bill </a></li>
                 <li class="nav-item"> <a href="{{ url('getview') }}" class="nav-link  text-dark">Single view</a></li> 
                <li class="nav-item"><a href="" class="nav-link  text-dark">search</a></li>-->

               <!----- <li class="nav-item"><a href="{{ url('getrecord') }}" class="nav-link  text-dark">single user Details</a></li>-->
            </ul>
            <button onclick="document.getElementById('id01').style.display='block'" style="width:auto;"><a href="log_in">Login</a></button>
            <button onclick="document.getElementById('id02').style.display='block'" style="width:auto;">register</button>
            
            <li</li>
            </ul>
        </div>
    </div>
</nav>
<div class="head">
    <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
          <div class="carousel-inner">
                <div class="carousel-item active">
                      <img src="../img/e4.jpg" class="d-block w-100" alt="pic">
                </div>
              <div class="carousel-item">
                      <img src="../img/e2.jpg" class="d-block w-100" alt="pic">
              </div>
              <div class="carousel-item">
                      <img src="../img/p16.jpg" class="d-block w-100" alt="pic">
              </div>
          </div>
    </div>
</div>
<button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls"  data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
</button>
<button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls"  data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
</button>
<!-- login/register -->



<!-- login pop up -->

<div id="id01" class="modal">
      <form class="modal-content animate" action="" method="post">
        <div class="imgcontainer">
          <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
              <img src="../img/1.png" alt="Avatar" class="avatar">
        </div>
    <div class="container">
          <label for="email"><b>email</b></label>
              <input type="email" placeholder="Enter email" name="email" required><br>
          <label for="password"><b>Password</b></label>
              <input type="password" placeholder="Enter Password" name="password" required>
              <input type="submit" value="Login" name="submit"><br><br>
              <div class="pass">
                    <span class="password">Forgot <a href="#">password?</a></span></div><br>
              <div >
          <label>
              <input type="checkbox" checked="checked" name="remember"> Remember me
          </label>
    </div>
          <div class="container" style="background-color:#f1f1f1">
                  <button type="button" onclick="document.getElementById('id01').style.display='none'" class="cancelbtn">Cancel</button>
                  
          </div>
      </div>
</form>


<!-- js -->
<script>
    var modal = document.getElementById('id01');

        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
                }
              }

      </script>
</div>
<!-- registration popup -->


<div id="id02" class="modal1">
      <span onclick="document.getElementById('id02').style.display='none'" class="close" title="Close Modal">&times;</span>
      <form class="modal-content" action="users" method="get">
<div class="container">
@csrf
@if(Session::get('success'))
<div class="alert alert-success">
{{Session::get('success')}}
</div>




@endif
@if(Session::get('fail'))
<div class="alert alert-danger">
{{Session::get('fail')}}
</div>
@endif

<h1>Register</h1>
              <p>Please fill in this form </p>
        <hr>
            <label for="consumerno"><b>Consumer No</b></label>
                <input type="text" placeholder="Enter consumer no" name="consumer_no" required class="cls"><br><br>
            <label for="name"><b>Name</b></label>
              <input type="text" name="name" placeholder="Name" pattern="[a-zA-Z]+" required="required" title="This field must be required" maxlength="25"  class="cls"><br><br>
              <label for="address"><b>Address</b></label>
                <input type="textarea" name="address" placeholder="Enter Your Address" required="" class="cls"></br></br>
           <label for="phno"><b>Phone number</b></label>
                <input type="text" name="phno" placeholder="Mobile number" required="" class="cls"><br>
            
              <label for="energycharge"><b>Energy charge</b></label>
              <input type="text" name="energy_charge" placeholder="energy charge" pattern="[0-9]+" required="required" title="This field must be required" maxlength="25"  class="cls"><br><br>
              <label for="duty"><b>Duty</b></label>
              <input type="text" name="duty" placeholder="duty" pattern="[0-9]+" required="required" title="This field must be required" maxlength="25"  class="cls"><br><br> 
              
              <label for="fixed_charge"><b>fixed charge</b></label>
                <input type="text" name="fixed_charge" placeholder="fixed charge" required="" class="cls"><br>
          <label for="meter_rent"><b>meter rent</b></label>
             <input type="text" placeholder="meter rent" name="meter_rent" required class="cls"><br><br>
             <label for="meterrent_stateGST"><b>meter rent STATE GST</b></label>
             <input type="text" placeholder="meterrent_stateGST" name="meterrent_stateGST" required class="cls"><br><br>
             <label for="meterrent_centralGST"><b>meter rent central GST</b></label>
             <input type="text" placeholder="meterrent_centralGST" name="meterrent_centralGST" required class="cls"><br><br>
             <label for="tariff"><b>tariff</b></label>
             <input list="tariff" name="tariff" placeholder="tariff" required="" class="cls"></br>
                   <datalist id="tariff">
                   <option value="LT-1A">
                   <option value="LT-2A">
                   <option value="LT-3A">
                   <option value="Other">
                   </datalist></br>
             <label for="purpose"><b>purpose</b></label>
            <input list="purpose" name="purpose" placeholder="purpose" required="" class="cls">
                   <datalist id="purpose">
                   <option value="Agriculture">
                   <option value="Residential">
                   <option value="Commercial">    
                   <option value="Other">
                   </datalist></br></br>
             
             <label for="billing_cycle"><b>billing cycle</b></label>
                <input type="radio" name="billing_cycle" id="onemonth" value=onemonth required="" >
            <label for="1month">one month</label>
                <input type="radio" name="billing_cycle" id="twomonth"  value=twomonth required="" >
            <label for="three">two month</label><br><br>
            <label for="consumed_units"><b>consumed units</b></label>
                <input type="text" name="consumed_units" placeholder="consumed_units" required="" class="cls"><br>
             <label for="phase"><b>phase</b></label>
                <input type="radio" name="phase" id="single" value=singlephase required="" >
            <label for="single">single phase</label>
                <input type="radio" name="phase" id="three"  value=threephase required="" >
            <label for="three">three phase</label><br><br>
            


          @if(Session::get('fail'))
  <div class="alert alert-danger">
  {{Session::get('fail')}}
  </div>
  @endif
  <label for="email"><b>Email</b></label>
                <input type="email" placeholder="Enter Email" name="email" required class="cls"><br><br>
          <label for="password"><b>Password</b></label>
                <input type="password" placeholder="Enter Password" name="password" pattern="(?=.*[!@#$%^&*])(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one special character and at least one number and one uppercase and lowercase letter, and at least 8 or more characters" required class="cls"><br>

        
          <input type="submit" name="submit"><br><br>
          <label>
            <input type="checkbox" checked="checked" name="remember" style="margin-bottom:15px"> Remember me
          </label>
                  <p>By creating an account you agree to our <a href="#" style="color:dodgerblue">Terms & Privacy</a>.</p>
              <div class="clearfix">
               <button type="button" onclick="document.getElementById('id02').style.display='none'" class="cancelbtn">Cancel</button>
            </div>
      </form>
</div>

<!-- js -->

<script>
  var modal = document.getElementById('id02');

                window.onclick = function(event) {
                      if (event.target == modal) {
                        modal.style.display = "none";
                          }
                            }
       
    </script>
  </section>



<!-- Trigger/Open The Modal -->


<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    
  </div>

</div>

 

<script>
// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
  modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>
  

  
    <!---Jquery--->
<script
  src="https://code.jquery.com/jquery-3.5.1.js"integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
  crossorigin="anonymous">
</script>

<!---Popper---->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous">
</script>

<!---Custom Js-->
<script src="js/script.js">

</script>
</body>     
</html>

